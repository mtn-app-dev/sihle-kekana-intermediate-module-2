//Declares which script is currently running.
console.log("\nScript One\n");

//Variables to store my personal data.
let myName = "Sihle";
let  institution = "University of South Africa (UNISA)";
let gitUserName = "SKekana";

//Statement to print my details to the console of the browser.
console.log("My name is " + myName + ", a student from " + institution + " and my Github username is " + gitUserName + ".");