//Declares which script is currently running.
console.log("\nScript Two\n");


//Definition of the Dice class that represent my dice.
class Dice{
        roll(){
                return Math.floor(Math.random()*6) + 1;
        };
}

//Create an instance of the Dice class and roll it twice, storing the result of each roll in a variable.
let dice = new Dice();
let rollOne = dice.roll();
let rollTwo = dice.roll();

let category = "";

//The winner is determined by the value of the two dice rolls.
switch(rollOne + rollTwo) {
        case 2: 
                category = "Best Consumer Solution";
                break;
        case 3: 
                category = "Best Enteprise Solution";
                break;
        case 4: 
                category = "Best African Solution";
                break;
        case 5: 
                category = "Most Innovative Solution";
                break;
        case 6: 
                category = "Best Gaming Solution";
                break;
        case 7: 
                category = "Best Health Solution";
                break;
        case 8: 
                category = "Best Agricultural Solution";
                break;
        case 9: 
                category = "Best Educational Solution";
                break;
        case 10: 
                category = "Best Finance Solution";
                break;
        case 11: 
                category = "Best Hackathon Solution";
                break;
        case 12: 
                category = "Best South African Solution";
                break;
        default:
                console.log("An error has occured.");
}

console.log("The category that will win the  MTN Business App of the Year in the year 2022 is " + category +".");