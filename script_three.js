//Declares which script is currently running.
console.log("\nScript Three\n");

let httpRequest = new XMLHttpRequest();
httpRequest.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200)
        {
                console.log("The data of the MTN App of the Year 2017 winner in \
                the Best Campus Cup category is as follows :\n\n" + this.responseText + "\n");
        }
        // console.log("readyState : " + this.readyState + "\nstatus: " + this.status + "\n");
};
httpRequest.open("GET","./data.txt",true);
httpRequest.send();

